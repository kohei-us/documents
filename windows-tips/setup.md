
Set up development environment
==============================

This is for people who prefer using Git Bash as the primary terminal on Windows,
and set it up so that it behaves similarly to GNOME Terminal.

Windows Terminal
----------------

### Add Git Bash terminal type

First and foremost, install [Microsoft Windows Terminal](https://aka.ms/terminal)
if you haven't already. Once installed, find the `settings.json` file located in

```bash
$LOCALAPPDATA/Packages/Microsoft.WindowsTerminal_{hash}/LocalState
```
where `{hash}` may be a random hash value.  Once found, add the following entry:

```json
{
  "guid": "{adec2407-0eec-4341-8e27-fee62dda0e6f}",
  "name": "Git Bash",
  "commandline": "\"%PROGRAMFILES%\\git\\bin\\bash.exe\" --login -i",
  "icon": "%PROGRAMFILES%\\git\\mingw64\\share\\git\\git-for-windows.ico",
  "startingDirectory": "%USERPROFILE%",
  "hidden": false
}
```

in the profiles list of `settings.json` in order for `Git Bash` to show up as a
terminal type.  The `guid` value can be any qualified UUID value.

### Add keyboard shortcuts for next-tab, previous-tab and duplicate-tab

Add the following:

```json
{ "command": "duplicateTab", "keys": "ctrl+shift+t" },
{ "command": "nextTab", "keys": "ctrl+pagedown" },
{ "command": "prevTab", "keys": "ctrl+pageup" }
```

in the actions list part of `settings.json`.

The duplicate-tab command currently does not start with the same directory as
the original terminal, but [it has been requested](https://github.com/microsoft/terminal/issues/3158).

Python Virtual Environment in Git Bash
--------------------------------------

This section describes how to set up Git Bash for Python virtual environment.

Let's assume you have installed your Python 3.8 to `C:\Python\x86_64\Python38`.
First, prepare the following file:

```bash
PYROOT=/c/Python/x86_64/Python38
PYROOTUSER=$(cygpath -u $APPDATA/Python/Python38)
export PATH=$PYROOTUSER/Scripts:$PYROOT:$PYROOT/Scripts:$PATH
```

Source this file after you start your bash session.  In case you have a newer
version of Python, simply replace the `38` suffix to `39` for Python 3.9, or to
`310` for Python 3.10, and so on.

First, run:

```
pip install virtualenv
```

to install `virtualenv` to the base installation.  Then run

```
python -m virtualenv /path/to/virtualenv
```

to create your virtual environment directory.  To activate your virtual
environment, run:

```
source /path/to/virtualenv/Scripts/activate
```

Add rsync
---------

Easiest way to add rsync to Git Bash is to install [MSYS2](https://msys2.org)
first by following the instructions on that page.  By default, MSYS2 gets installed to
`C:\msys64`.  Once installed, go far enough to install rsync via `pacman` in your
MSYS2 environment.

Next, find the following files:

* rsync.exe
* msys-xxhash-0.dll
* msys-zstd-1.dll

in `C:\msys64\user\bin`, and copy them into `C:\Program Files\Git\usr\bin`.
