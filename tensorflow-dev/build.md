
## Install Bazel

Download the latest from https://github.com/bazelbuild/bazel/releases, and
pick the installer that has the following pattern:

```
bazel-<version>-installer-linux-x86_64.sh
```

```bash
chmod +x bazel-...-installer-linux-x86_64.sh
sudo ./bazel-...-installer-linux-x86_64.sh --prefix=/opt/bazel
```

The bezel command is now available at `/opt/bazel/bin/bazel`.  Make sure this
command is available in your PATH.

## Clone tensorflow repository

```bash
git clone https://github.com/tensorflow/tensorflow.git
cd tensorflow
git checkout r1.11
./configure
```
Specify as the compiler options `-march=native` in order to get CPU's AVX2
instruction set enabled.

```bash
bazel build --config=opt //tensorflow/tools/pip_package:build_pip_package
```

```bash
bazel build --config=opt //tensorflow:libtensorflow.so
```


